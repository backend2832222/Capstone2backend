const jwt = require('jsonwebtoken');
const User = require('./models/User');

const verifyToken = async (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    const decoded = jwt.verify(token, 'secret');


    const user = await User.findById(decoded.userId);
    if (!user) {
      return res.status(401).json({ message: 'Authentication failed' });
    }
    req.user = {};
    req.user.userId = user._id;
    req.user.isAdmin = user.isAdmin;
    next();
  } catch (err) {
    res.status(401).json({ message: 'Authentication failed' });
  }
};


module.exports = verifyToken;
