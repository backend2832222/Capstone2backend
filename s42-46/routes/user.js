const express = require('express');
const router = express.Router();
const verifyToken = require('../auth');

const { 
  registerUser, 
  loginUser, 
  createProduct, 
  getAllProducts,
  getActiveProducts,
  getProduct, 
  updateProduct, 
  archiveProduct,
  checkout, 
  getUserDetails,
  setUserAsAdmin,
  deleteAllOrders
} = require('../controller/user');

const requireAdmin = async (req, res, next) => {
  try {
    const user = req.user;
    if (user && user.isAdmin) {
      next();
    } else {
      res.status(403).send({ message: 'You do not have permission to perform this action.' });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Internal server error' });
  }
};

router.post('/register', registerUser);
router.post('/products', verifyToken, requireAdmin, createProduct);
router.post('/login', loginUser);
router.get('/products', getAllProducts); 
router.get('/products/active', getActiveProducts); 
router.get('/products/:id', getProduct);
router.put('/products/:id', verifyToken, requireAdmin, updateProduct); 
router.patch('/products/:id/archive', verifyToken, requireAdmin, archiveProduct); 
router.post("/checkout", verifyToken, checkout);
router.get("/details", verifyToken, getUserDetails);
router.post("/setAdmin", verifyToken, requireAdmin, setUserAsAdmin);
router.delete("/deleteAllOrders", verifyToken, deleteAllOrders);

module.exports = router;
