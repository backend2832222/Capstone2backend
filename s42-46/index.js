const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require('./routes/user'); // Adjusted the path

const app = express();

mongoose.connect("mongodb+srv://patrick-255:admin123@zuitt-bootcamp.a8lpb2y.mongodb.net/?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

// Register user routes
app.use('/users', userRoutes);

if(require.main === module){
    app.listen(process.env.PORT || 4000, () => {
        console.log(`API is now online on port ${ process.env.PORT || 4000}`)
    });
}

module.exports = app;
